FROM tomcat:8.0-alpine
ADD spring-mvc-maven.war /usr/local/tomcat/webapps/

EXPOSE 8085
CMD ["catalina.sh", "run"]
